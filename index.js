const { exec } = require("child_process");
const express = require("express");
require("dotenv").config();
const app = express();

app.post("/", (_, res) => {
    exec(
        `cd "${process.env.BOTPATH}" && git pull && docker-compose up -d --build`,
    );
    res.sendStatus(200);
});

const port = 5074;
app.listen(port, "127.0.0.1", () => {
    console.log(`Listening on port ${port}`);
});
